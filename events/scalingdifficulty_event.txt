namespace = scalingdifficulty

country_event = {
	id = scalingdifficulty.1
	title = "SCALINGDIFFICULTY_NAME"
	desc = "SCALINGDIFFICULTY_DESC"
	picture = BATTLE_eventPicture
	hidden = yes

	mean_time_to_happen = {
		months = 1
	}

	immediate = {
		remove_country_modifier = scalingdifficulty.1_active
		remove_country_modifier = scalingdifficulty.2_active
		remove_country_modifier = scalingdifficulty.3_active

		if = {
			limit = {
				ai = yes
				current_age = age_of_reformation
			}
			add_country_modifier = {
				name = scalingdifficulty.1_active
				duration = -1
			}
		}
		if = {
			limit = {
				ai = yes
				current_age = age_of_absolutism
			}
			add_country_modifier = {
				name = scalingdifficulty.2_active
				duration = -1
			}
		}
		if = {
			limit = {
				ai = yes
				current_age = age_of_revolutions
			}
			add_country_modifier = {
				name = scalingdifficulty.3_active
				duration = -1
			}
		}
	}

	option = {
		name = "SCALINGDIFFICULTY.1_OPTION1"
		ai_chance = { factor = 100 }
	}
}