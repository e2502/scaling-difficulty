scalingdifficulty.1_active = { #Age of Reformation
	global_tax_modifier = 0.10
	production_efficiency = 0.10
	trade_efficiency = 0.10

	global_manpower_modifier = 0.10
	global_sailors_modifier = 0.10

	land_forcelimit_modifier = 0.10
	naval_forcelimit_modifier = 0.10

	development_cost = -0.05
}
scalingdifficulty.2_active = { #Age of Absolutism
	global_tax_modifier = 0.30
	production_efficiency = 0.30
	trade_efficiency = 0.30

	global_manpower_modifier = 0.30
	global_sailors_modifier = 0.30

	land_forcelimit_modifier = 0.30
	naval_forcelimit_modifier = 0.30

	development_cost = -0.10

	country_admin_power = 1
	country_diplomatic_power = 1
	country_military_power = 1
}
scalingdifficulty.3_active = { #Age of Revolutions
	global_tax_modifier = 0.60
	production_efficiency = 0.60
	trade_efficiency = 0.60

	global_manpower_modifier = 0.60
	global_sailors_modifier = 0.60

	land_forcelimit_modifier = 0.60
	naval_forcelimit_modifier = 0.60

	development_cost = -0.20

	country_admin_power = 2
	country_diplomatic_power = 2
	country_military_power = 2
}